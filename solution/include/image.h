#ifndef HEADER_IMAGE
#define HEADER_IMAGE
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
struct image {
    uint64_t width, height;
    struct pixel *data;
};
#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)
void destroy(struct image* img);
struct image* make_image(uint64_t width, uint64_t height);
#endif
