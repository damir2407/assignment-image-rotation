#include "../include/image.h"
#include <inttypes.h>
#include <stdlib.h>



struct image* make_image(uint64_t width, uint64_t height) {
    struct image *new_image = malloc(sizeof(struct image));
    new_image->width = width;
    new_image->height = height;
    new_image->data = malloc(sizeof(struct pixel) * width * height);
    return new_image;
}
void destroy(struct image* img){
    free(img->data);
    free(img);
}
