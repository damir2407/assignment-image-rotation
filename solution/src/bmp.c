#define BFTYPE 0x4D42
#define BFRESERVED 0
#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BICOMPRESSION 0
#define BIXPELSPERMETER 0
#define BIYPELSPERMETER 0
#define BICLRUSED 0
#define BICLRIMPORTANT 0
#include "../include/image.h"
#include "../include/bmp.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
enum read_status from_bmp( FILE* in, struct image** img ){
  bmp_header header;
  fread(&header, sizeof(bmp_header), 1, in); 
  
  uint8_t margin=((3*header.biWidth) % 4);
  margin = (margin==0) ?(margin) : (4-margin);
   
  struct pixel pix;
  *img = make_image(header.biWidth, header.biHeight);
  
  uint32_t x=0;
  uint32_t y=0;
  while(x<header.biHeight){
    while (y<header.biWidth) {
      fread(&pix, sizeof(struct pixel), 1, in);
      (*img)->data[header.biWidth * x + y] = pix;
      y=y+1;
    }
	x=x+1;
	y=0;
    if (margin != 0){
      fread(&pix, sizeof(unsigned char)*margin, 1, in);
    }
  }
  if(header.biBitCount != 24){
    return READ_INVALID_BITS;
  }
  return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
  size_t padding = (4 - (img->width * sizeof(struct pixel))) % 4;
  uint32_t img_size = (img->width * sizeof(struct pixel) + padding) * img->height;
  bmp_header header={0};
  struct pixel pix;
  header.bfType = BFTYPE;
  header.bfileSize = sizeof(bmp_header)+header.biSizeImage;
  header.bfReserved = BFRESERVED;
  header.bOffBits = sizeof(bmp_header);
  header.biSize = BISIZE;
  header.biWidth = img->width;
  header.biHeight = img->height;
  header.biPlanes = BIPLANES;
  header.biBitCount = BIBITCOUNT;
  header.biCompression = BICOMPRESSION;
  header.biSizeImage = img_size;
  header.biXPelsPerMeter = BIXPELSPERMETER;
  header.biYPelsPerMeter = BIYPELSPERMETER;
  header.biClrUsed = BICLRUSED;
  header.biClrImportant = BICLRIMPORTANT;

  fwrite(&header, sizeof(bmp_header), 1, out);
  fseek(out, header.bOffBits, SEEK_SET);
  uint8_t margin = (4 - (sizeof(struct pixel)* header.biWidth)) % 4;
  uint64_t x=0;
  uint64_t y=0;
  while (x<img->height) {
    while (y<img->width) {
      pix = img->data[img->width * x + y];
      fwrite(&pix, sizeof(struct pixel), 1, out);
      y=y+1;
    }
    x=x+1;
    y=0;
    if (margin != 0) {
     uint8_t i=0;
      while (i<margin) {
        i=i+1;
        fwrite(&pix, sizeof(char), 1, out);
      }
    }else return WRITE_ERROR;
  }
  return WRITE_OK;

}
