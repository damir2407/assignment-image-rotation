#include "../include/image.h"
struct image* rotate(struct image const *source){
  struct image* rotate_image = make_image(source->height, source->width);
  uint64_t y=0;
  uint64_t x=0;
  while(y<source->height){
    while(x<source->width){
      rotate_image->data[rotate_image->width*x+source->height-y-1] =
      source->data[source->width*y+x];
      x=x+1;
    }
    y=y+1;
    x=0;
  }
  return rotate_image;
}
