#include "../include/file_work.h"
#include <stdio.h>
#include <stdlib.h>

bool file_open(FILE** file, const char *path_name, char* mode) {
    if (path_name==NULL) return false;
    *file = fopen(path_name, mode);
    if (*file != NULL) {
        return true;
        fclose(*file); 
    } else return false;
}
