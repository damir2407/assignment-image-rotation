#include "../include/bmp.h"
#include "../include/file_work.h"
#include "../include/transform.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {

char* read_status_describe[]= {
    [READ_OK] = "Success, file loaded successfuly",
    [READ_INVALID_SIGNATURE]="Error, file has invalid signature",
    [READ_INVALID_BITS]="Error, file has invalid bits",
    [READ_INVALID_HEADER]="Error, file has invalid header"
};
char* write_status_describe[]= {
    [WRITE_OK] = "Success, image loaded successfully",
    [WRITE_ERROR]="Error, image was not loaded"
};
char* global_status[]= {
    [ARGUMENTS_ERROR] = "The number of arguments passed must be 2!\n",
    [NOT_FOUND_ERROR] = "Source file not found!\n",
    [SUCCESS_OPEN] = "Source file successfully opened!\n"
};


    if (argc < 3) {
        printf("%s",global_status[ARGUMENTS_ERROR]);
        return 0;
    }
    
//input part
    FILE *inputFile = NULL;
    struct image *inputImage = {0};
    if (!file_open(&inputFile, argv[1],"rb")) {
     	 printf("%s",global_status[NOT_FOUND_ERROR]);
     	return 0;
     }else {
     	printf("%s",global_status[SUCCESS_OPEN]);
     }
    enum read_status read_condition = from_bmp(inputFile, &inputImage);
    if (read_condition==READ_OK) {	
    	printf("%s",read_status_describe[READ_OK]);
    }else {
        printf("%s",read_status_describe[read_condition]);
        return 0;
       }
    FILE *outputFile = NULL;
    struct image *outputImage = {0};
    outputImage = rotate(inputImage);
    destroy(inputImage);

//output part
    if (file_open(&outputFile, argv[2],"wb")) {
    	printf("%s",global_status[NOT_FOUND_ERROR]);
    }else{
    	printf("%s",global_status[SUCCESS_OPEN]);
        return 0;
    }
    enum write_status write_condition = to_bmp(outputFile, outputImage);
    if (write_condition==WRITE_OK) {
        printf("%s",write_status_describe[WRITE_OK]);
        }else{
         printf("%s",write_status_describe[write_condition]);
        return 0;
        }
    destroy(outputImage);
    return 0;
}
